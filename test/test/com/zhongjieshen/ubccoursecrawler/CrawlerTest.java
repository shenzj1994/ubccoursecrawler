package test.com.zhongjieshen.ubccoursecrawler;

import com.zhongjieshen.ubccoursecrawler.Crawler;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class CrawlerTest {

    private Crawler c = new Crawler();

    private static final int MIN_TIMEOUT = 100;

    @Test
    void runOnePage() {
        c.runOnRemotePage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=MUSC&course=407C&section=001");
        c.runOnRemotePage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=ARTH&course=432&section=001");
        c.runOnRemotePage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=BA&course=564&section=004");
        c.runOnRemotePage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=CNPS&course=588C&section=004");
        c.runOnRemotePage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=COMM&course=390&section=120");

    }

    @Test
    void runFullSearch() {
        c.runFullSearch();
    }

    @Test
    void listAllUrlsInPage(){
        c.listAllUrlsInPage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=0","dept=");
        c.listAllUrlsInPage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=1&dept=AANB","course=");
        c.listAllUrlsInPage("https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=3&dept=AANB&course=500","section=");
    }

    @Test
    void getAllSectionsUrls(){
        c.getAllSectionsUrls();
    }

}