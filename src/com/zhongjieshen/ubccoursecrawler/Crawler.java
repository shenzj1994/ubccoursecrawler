package com.zhongjieshen.ubccoursecrawler;

import com.opencsv.CSVWriter;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Crawler {

    private final String topUrl = "https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=0";
    private final String logPath = "E:\\" + LocalDate.now() + ".log";
    private final String sectionUrlsPath = "E:\\sectionUrls." + LocalDate.now() + ".txt";
    private final String csvPath = "E:\\full_search." + LocalDate.now() + ".csv";

    private final String tableName = "ubc_course";
    private final String dbUrl = "jdbc:mysql://db01.zhongjieshen.com:3306/jdbc_test?&useSSL=false";
    //private final String dbUrl = "jdbc:mysql://localhost:3306/jdbc_test?&useSSL=false";

    private final String dbUser = "java_user";
    private final String dbPassword = "user_java";


    private final boolean LOG_ENABLED = false;


    public void runOnRemotePage(String url) {
        try {
            searchAClass(getFromUrl(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runFullSearch() {
        try {
            java.sql.Connection connection = setUpSqlConnection();

            //Writer w = new FileWriter(new File(csvPath));
            List<String> temp = new ArrayList<>();
            List<String> sqlTemp = new ArrayList<>();

            List<String> subjectsUrl = listAllUrlsInUBC(topUrl);
            if (subjectsUrl != null) {
                for (String aSubject : subjectsUrl) {
                    List<String> coursesUrl = listAllUrlsInASubject(aSubject);
                    if (coursesUrl != null) {
                        for (String aCourse : coursesUrl) {
                            List<String> sectionsUrl = listAllUrlsInACourse(aCourse);
                            if (sectionsUrl != null) {
                                for (String aSectionUrl : sectionsUrl) {
                                    Document targetDoc = getFromUrl(aSectionUrl);
                                    temp.add(aSectionUrl);
                                    temp.addAll(searchAClass(targetDoc));
                                }
                            }
                        }
                        insertToSql(connection, temp);
                        //writeToCsv(w, temp.toArray(new String[0]));
                        temp.clear();
                    }
                }
            }

            //w.close();
            connection.close();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void getAllSectionsUrls() {
        List<String> subjectsUrl = listAllUrlsInUBC(topUrl);
        if (subjectsUrl != null) {
            for (String aSubject : subjectsUrl) {
                List<String> coursesUrl = listAllUrlsInASubject(aSubject);
                if (coursesUrl != null) {
                    for (String aCourse : coursesUrl) {
                        List<String> sectionsUrl = listAllUrlsInACourse(aCourse);
                        if (sectionsUrl != null) {
                            for (String aSectionUrl : sectionsUrl) {
                                System.out.println(aSectionUrl);
                                writeToFile(new File(sectionUrlsPath), aSectionUrl + System.lineSeparator());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Download and clean the doc from an URL.
     *
     * @param url Target URL.
     * @return A cleaned doc.
     * @throws IOException When it failed to download a doc (e.g. Timeout).
     */
    private Document getFromUrl(String url) throws IOException {
        Document doc = Jsoup.connect(url)
                .timeout(10000)
                .ignoreHttpErrors(true)
                .ignoreContentType(true)
                .followRedirects(true)
                .get();

        return cleanUpADoc(doc);
    }

    /**
     * Find the <b>Course name</b> in a <b>Document</b>.
     *
     * @param doc The Document
     * @return A string of the course name or the document url if no course name presents.
     */
    private String findCourseName(Document doc) {
        String name = doc.getElementsByTag("h4").text();
        if (!name.isEmpty()) {
            writeToLog(name + System.lineSeparator());
            System.out.println("Searching in: " + name);
            return name;
        } else
            return doc.location();
    }

    /**
     * Find the number of seats in a HTML Document.
     *
     * @param doc      The Document needs to be searched.
     * @param keywords The array of keywords to search.
     * @return The list in a format of: keyword + number of seats
     */
    private List<String> findNumOfSeats(Document doc, String[] keywords) {
        Elements elementsHaveKeyword;
        Element finalNumberElement;
        List<String> result = new ArrayList<>();

        //For each keyword in the keywords array
        for (String keyword : keywords) {
            String numOfSeats;
            elementsHaveKeyword = doc.getElementsContainingOwnText(keyword);
            if (elementsHaveKeyword.size() != 0) {
                Element element = elementsHaveKeyword.first();
                finalNumberElement = element.nextElementSibling(); //Select the sibling of keyword(e.g. 'Total seats remaining')

                numOfSeats = extractNumber(finalNumberElement.text());
                if (numOfSeats.isEmpty())
                    numOfSeats = "-1";
            } else {
                numOfSeats = "-1";
            }
            //Process results
            result.add(numOfSeats);
            writeToLog(keyword + numOfSeats + System.lineSeparator());
            System.out.println(keyword + numOfSeats);
        }
        return result;
    }

    /**
     * List all the urls in the top-level page (The page with the <b>department code</b>).
     *
     * @param url url to the page.
     * @return A string list of all the urls
     */
    private List<String> listAllUrlsInUBC(String url) {
        return listAllUrlsInPage(url, "dept=");
    }

    /**
     * List all the urls in the department/subject page (The page with <b>course</b> numbers).
     *
     * @param url url to the page.
     * @return A string list of all the urls
     */
    private List<String> listAllUrlsInASubject(String url) {
        return listAllUrlsInPage(url, "course=");
    }

    /**
     * List all the urls in a course page (The page with <b>section</b> numbers).
     *
     * @param url url to the page.
     * @return A string list of all the urls
     */
    private List<String> listAllUrlsInACourse(String url) {
        return listAllUrlsInPage(url, "section=");
    }

    /**
     * List all the urls in a page. The url must contains the keyword.
     * <p>It only extracts the 'href' attributes under 'td' tags.</p>
     *
     * @param url     url to the page.
     * @param keyword The keyword that urls must have.
     * @return A string list of all the urls
     */
    public List<String> listAllUrlsInPage(String url, String keyword) {
        try {
            Document doc = getFromUrl(url);
            Elements elements = doc.select("td>a[href]");
            List<String> urls = new ArrayList<>();
            for (Element e0 : elements) {
                if (e0.attr("href").contains(keyword)) {
                    String u = e0.attr("abs:href");
                    urls.add(cleanUpAUrl(u));
                }
            }
            System.out.println(urls);
            return urls;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Clean up an URL string. Remove <b>jsessionid</b> and <b>amp;</b>.
     *
     * @param url An URL string to be cleaned up.
     * @return A clean URL string.
     */
    private String cleanUpAUrl(String url) {
        url = url.replaceAll(";jsessionid=(.*)\\?", "?");
        url = url.replaceAll("amp;", "");

        return url;
    }

    /**
     * Clean up a Document with defined whitelist
     *
     * @param document Document to be cleaned
     * @return A cleaned Document.
     */
    private Document cleanUpADoc(Document document) {
        Whitelist wl = new Whitelist();
        wl = Whitelist.basic(); //Use basic whitelist first

        //Add two retaining tags
        wl.addTags("h4");
        wl.addTags("td");

        Cleaner cl = new Cleaner(wl);
        return cl.clean(document);
    }

    /**
     * Extract the numbers in a string by removing everything else.
     * @param s A String
     * @return A String contains number only
     */
    private String extractNumber(String s) {
        return s.replaceAll("[^0-9]+", "");
    }

    /**
     * Search course name and seats in a class. (Like APSC 201 101)
     * @param doc A Document which is the class page.
     * @return A String list contains url, course name, and seats numbers.
     */
    private List<String> searchAClass(Document doc) {
        List<String> temp = new ArrayList<>();
        temp.add(findCourseName(doc));
        String[] keywords = {"Total Seats Remaining:", "Currently Registered:", "General Seats Remaining:", "Restricted Seats Remaining*:"};
        temp.addAll(findNumOfSeats(doc, keywords));
        return temp;
    }

    /**
     * Append String to the log file
     * @param s String to be appended
     */
    private void writeToLog(String s) {
        if (LOG_ENABLED) {
            writeToFile(new File(logPath), s);
        }
    }

    /**
     * Append a string to a file.
     * @param file A file to be written.
     * @param s A string to write to file.
     */
    private void writeToFile(File file, String s) {
        try {
            FileUtils.write(file, s, "UTF-8", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    private void writeToCsv(Writer w, String[] line) throws IOException {
        CSVWriter csvW = new CSVWriter(w);
        csvW.writeNext(line);
        csvW.flush();
    }

    /**
     * Insert the content of a String list to SQL database
     *
     * @param connection A SQL connection
     * @param list       A list with the rows to be inserted.
     * @throws SQLException When it encounters a SQL Exception
     */
    public void insertToSql(java.sql.Connection connection, List<String> list) throws SQLException {
        String values = prepareAllValues(list);

//        StringBuilder s= new StringBuilder();
//        for (int i=0;i<10000;i++){
//            s.append(values);
//            if (i<9999)
//                s.append(",");
//        }
//        values=s.toString();
        if (!values.isEmpty()) {
            String sqlQ =
                    "INSERT INTO "
                            + tableName
                            + " (`url`, `name`, `tsr`, `cr`, `gsr`, `rsr`) VALUES "
                            + values
                            + ";";
            writeToLog("SQL: " + sqlQ + System.lineSeparator());
            fireSqlStmt(connection, sqlQ);
        }
    }

    /**
     * Prepare the insert values in a way like <i>('1','2'),('3','4')</i>.
     *
     * @param list A list contains all the single values like <i>['1','2','3','4']</i>.
     * @return A String with all the insert values in a formatted way like <i>('1','2'),('3','4')</i>.
     */
    private String prepareAllValues(List<String> list) {
        StringBuilder s = new StringBuilder();
        List<String> temp = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            temp.add(list.get(i));
            if (temp.size() == 6) { //When the temp list has 6 strings
                s.append(prepareOneSetOfValues(temp));
                if (i < (list.size() - 1))
                    s.append(",");
                temp.clear();
            }
        }
        return s.toString();
    }

    /**
     * Prepare one set of insert values in a way like <i>('1','2')</i>.
     *
     * @param list A list contains all the single values like <i>['1','2']</i>.
     * @return A String with all the insert values in a formatted way like <i>('1','2')</i>.
     */
    private String prepareOneSetOfValues(List<String> list) {
        StringBuilder data = new StringBuilder();
        data.append("(");
        for (int i = 0; i < list.size(); i++) {
            data.append("'").append(list.get(i)).append("'");
            if (i < list.size() - 1) {
                data.append(",");
            }
        }
        data.append(")");
        return data.toString();
    }

    /**
     * Execute a SQL Statement
     *
     * @param connection A SQL Connection
     * @param sql        A String has SQL statement
     * @throws SQLException When it encounters a SQL Exception
     */
    public void fireSqlStmt(java.sql.Connection connection, String sql) throws SQLException {
        Statement statement = connection.createStatement();
        System.out.println(sql);
        statement.executeUpdate(sql);
    }

    /**
     * Set up a SQL connection
     * @return A SQL Connection
     * @throws SQLException When it encounters a SQL Exception
     */
    private java.sql.Connection setUpSqlConnection() throws SQLException {

        java.sql.Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        System.out.println("Connected to: " + dbUrl);
        System.out.println("User: " + dbUser);

        return connection;
    }
}
