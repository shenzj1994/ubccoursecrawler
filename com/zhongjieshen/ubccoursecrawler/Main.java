package com.zhongjieshen.ubccoursecrawler;

public class Main {

    public static void main(String[] args) {
        // write your code here
        long time_0 = System.currentTimeMillis();

        Crawler c = new Crawler();
        c.runFullSearch();


        long time_1 = System.currentTimeMillis();
        System.out.println("Time used: " + (time_1 - time_0));

    }
}
